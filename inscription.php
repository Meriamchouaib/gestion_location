
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <link href="style/inscription.css" rel="stylesheet" >
  

</head>
<body>
<?php include "header.php"; ?>
<?php

$pdo = new PDO('mysql:host=localhost;port=3306;dbname=gestion_location', 'root', '');
$pdo ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$errors = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$adresse = $_POST['adresse'];
$password = $_POST['password'];
$email = $_POST['email'];
$numTel = $_POST['numTel'];


    if(!$nom){
        $errors[] = 'insérer votre <b> nom </b> SVP!!';
    }
    if(!$prenom){
        $errors[] = 'insérer votre <b> prénom </b> SVP!!';
    }    
    if(!$adresse){
        $errors[] = 'insérer votre <b> adresse </b> SVP!!';
    } 
    if(!$password){
        $errors[] = 'insérer votre <b> mot de passe </b> SVP!!';
    } 
    if(!$email){
        $errors[] = 'insérer votre <b> email </b> SVP!!';
    } 
    if(!$numTel){
        $errors[] = 'insérer votre <b> numéro de téléphone </b> SVP!!';
    }  
    
if (empty($errors)){
   

    $statement = $pdo->prepare("INSERT INTO utilisateur ( NomU, PrenomU, AdrU, NumU, PasswdU, emailU)
    VALUES (:nom, :prenom, :adresse, :numTel, :password, :email)"
   );
/*$pdo->exec("INSERT INTO locataire ( Nom, Prenom)
             VALUES ('$nom','$prenom')"
           );*/
    $statement->bindValue(':nom', $nom);
    $statement->bindValue(':prenom', $prenom);   
    $statement->bindValue(':adresse', $adresse); 
    $statement->bindValue(':numTel', $numTel); 
    $statement->bindValue(':password', $password); 
    $statement->bindValue(':email', $email); 
    $statement->execute();   
     
    header ("Location: create_annonce.php");

        }
    }

?>
<div class="container">
    
<form class="row g-3 form1" action="inscription" method="post">
<div class="col-md-6">
    <input type="text" class="form-control" placeholder="First name" aria-label="First name" name="nom">
  </div>
  <div class="col-md-6">
    <input type="text" class="form-control" placeholder="Last name" aria-label="Last name" name="prenom">
  </div>
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">Email</label>
    <input type="email" class="form-control" id="inputEmail4" name="email">
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">Password</label>
    <input type="password" class="form-control" id="inputPassword4" name="password">
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="adresse">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">phone number</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="52489325" name="numTel">
  </div>
  
  <div class="col-12">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        Check me out
      </label>
    </div>
  </div>
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Sign in</button>
  </div>
</form>

</div>


<?php include "footer.php"; ?>
</body>
</html>