
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nouveau Locataire</title>
  

</head>
<body>
<?php include "header.php";

$pdo = new PDO('mysql:host=localhost;port=3306;dbname=gestion_location', 'root', '');
$pdo ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$errors = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];


    if(!$nom){
        $errors[] = 'insérer votre <b> nom </b> SVP!!';
    }
    if(!$prenom){
        $errors[] = 'insérer votre <b> prénom </b> SVP!!';
    }     
    
if (empty($errors)){

    $statement = $pdo->prepare("INSERT INTO locataire ( Nom, Prenom)
    VALUES (:nom, :prenom)"
   );
/*$pdo->exec("INSERT INTO locataire ( Nom, Prenom)
             VALUES ('$nom','$prenom')"
           );*/
    $statement->bindValue(':nom', $nom);
    $statement->bindValue(':prenom', $prenom);   
    $statement->execute();   
    

        }
    }

?>
<div class="container">

  <h1> Création d'un nouveau locataire </h1>

  <?php if (!empty($errors)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errors as $error): ?>
                <div> <?php echo $error; ?> </div>
            <?php endforeach; ?>
        </div>
  <?php endif; ?>
  <!--<div class="input-group">
    <span class="input-group-text">First and last name</span>
    <input type="text" aria-label="First name" class="form-control">
    <input type="text" aria-label="Last name" class="form-control">

  </div>-->
  <form action="create_locataire" method="post">
        <div class="form-group">
            <label>Nom</label>
            <input type="text" class="form-control" name="nom">
        </div>
        <div class="form-group">
            <label>Prenom</label>
            <input type="text" class="form-control" name="prenom">
        </div>
        <button type="submit" class="btn btn-primary">Valider</button>
    </form>
</div>


<?php include "footer.php"; ?>
</body>
</html>