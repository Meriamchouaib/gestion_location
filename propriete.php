<?php

$pdo = new PDO('mysql:host=localhost;port=3306;dbname=gestion_location','root','');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$statement = $pdo->prepare('SELECT * FROM locataire ORDER BY nom');
$statement->execute();
$locataire = $statement->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proprietes</title>
  

</head>
<body>
<?php include "header.php"; ?>
<div class="container">
    <p>
        <a type="button" class="btn btn-success" href="Create_locataire.php">Nouveau Locataire</a>
    </p>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">idLoc</th>
      <th scope="col">Nom</th>
      <th scope="col">Prenom</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($locataire as $i => $locatair) {?>
    <tr>
        <th scope="row"><?php echo $i+1 ?></th>
        <td><?php echo $locatair['idLoc'] ?></td>
        <td><?php echo $locatair['Nom'] ?></td>
        <td><?php echo $locatair['Prenom'] ?></td>
       <td> <button type="button" class="btn btn-primary">modifier</button> </td>
       <td> <button type="button" class="btn btn-danger">Supprimer</button> </td>

    </tr>

 <?php } ?>  
  </tbody>
</table>

</div>


<?php include "footer.php"; ?>
</body>
</html>