<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link href="style/connexion.css" rel="stylesheet" >
  

</head>
<body>
<?php include "header.php";?>
<div class="container part1">
    <div class="row">
        <div class="col-md-8 align-self-center">
          <h1 class="titre">Connectez vous!</h1>
        </div>
       </div>
        <div class="row" >
            <div class="col-md-6">
          <form action="verification.php" method="POST">
                <h1>Connexion</h1>
                
                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>

                <input type="submit" id='submit' value='LOGIN' >
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
                  <a href="inscription.php">Inscrivez vous Maintenant!</a>
            </form>
          
            </div>
        </div>
       

       
</div>

<?php include "footer.php"; ?>
</body>
</html>